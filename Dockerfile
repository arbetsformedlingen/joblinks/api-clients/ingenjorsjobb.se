FROM docker.io/library/debian:stable-20240926-slim as base

RUN apt-get -y update \
    && apt-get -y install --no-install-recommends html2text curl jq yq \
    && apt-get -y clean \
    && rm -rf /var/cache/apt

WORKDIR /app
COPY src/get.sh /app/get.sh
COPY example-data/input0.xml /app/mock.xml
RUN chmod +x /app/get.sh

FROM base as test
RUN /app/get.sh -h && touch /tests-successful

FROM base as final
COPY --from=test /tests-successful /
ENTRYPOINT [ "/app/get.sh" ]
