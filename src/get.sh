#!/usr/bin/env bash
set -eEu -o pipefail

# this script's path
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# the endpoint to consume
endpoint="https://ingenjorsjobb.se/xml"


# if not set, fetch from API with curl
local_input_file=



# process flags
while [ "$#" -gt 0 ]; do
    case "$1" in
        --mock|--no-fetch) local_input_file="$self_dir"/mock.xml;;
        -i|--input)        local_input_file="$2"; shift;;
        -h|--help)         echo "Usage: $0 [--mock|--no-fetch] [-i|--input file] [-h|--help]" >&2; exit 0;;
        *)                 echo "**** unknown option $1" >&2; exit 1;;
    esac
    shift
done



# Source data and print to stdout. Optional argument: a file name to read from,
# otherwise read from network (the API).
function source_data() {
    local inputfile="$1"

    if [ -n "$inputfile" ]; then
        if [ -s "$inputfile" ] || [ "$inputfile" = "-" ]; then
            echo "using $inputfile" >&2
            cat "$inputfile"
        else
            echo "**** verify file $inputfile" >&2; exit 1
        fi
    else
        curl -s --retry 3 -L "$endpoint"
    fi
}



function transform_data() {
    # Extract three fields from the input XML: title, description and url.
    # Line separator: \0. Field separator \1.
    xq -jrc '.source.job[] | [.title, "\u0001", .description, "\u0001", .url, "\u0000" ][]' |\

        # process each line (use \0 and \1 as separators), and assign fields to variables: title description url
        while IFS=$'\1' read -r -d $'\0' title description url; do

            # clean up the description data:
            desc=$(echo "$description" |\

                       # format html into text
                       html2text -utf8 -style pretty |\

                       # replace newline characters with space
                       sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/ /g' |\

                       # escape double quotes, as it would terminate the JSON string
                       sed 's|"|\\"|g' )

            # echo a JSONL JobPosting line
            echo '{ "@type": "JobPosting", "@context": "http://schema.org/", "title": "'"$title"'", "description": "'"$desc"'", "url": "'"$url"'" }'
        done
}



# MAIN
source_data "$local_input_file"  | # 1. source data \
    transform_data               | # 2. transform data to jobposting \
    jq --monochrome-output -c .    # 3. json validation
