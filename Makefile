IMG = apiclient-ingenjorsjobb.se

run:
	@docker run --rm -i $(IMG)

test:
	@docker run --rm -i $(IMG) --mock

build:
	docker build --no-cache -t $(IMG) -f Dockerfile .
